# Gradle Java Testing Docker Environment

A Docker container environment for building and testing Java applications using Gradle interactively with a real-time editable project.

This repository also contains a sample project (`/project`) ready to build including a faulty test to validate correct operation of Gradle. I recommend building the sample project first, then applying your own after everything is functional.

## Usage

### 1. Install Docker

> ℹ️ This method only works for Windows using winget. Linux users can reference [this guide](https://docs.docker.com/engine/install/).

```powershell
winget install Docker.DockerDesktop
```

Then start Docker service. You might need to do some tinkering with virtual environment. Here are the official instructions:

https://docs.docker.com/desktop/windows/install/

### 2. Fork this repository, then download

> ℹ️ Alternatively you may just clone it

```powershell
git clone https://gitlab.com/richardnagy/container-environments/gradle-java-testing-docker-environment
cd gradle-java-testing-docker-environment
```

### 3. Build docker image

> ℹ️ You may change the name `sw-testing` to your preference, but do remember to use the same one furher on

```powershell
docker build -t sw-testing .
```

This will take a couple of minutes depending on your system.

### 4. Run container

> ℹ️ Make sure you are using PowerShell, not cmd as some commands will fail in cmd.

```powershell
docker run -d -v ${pwd}/project:/usr/project sw-testing:latest
```

Take the output running container hash and add it to the next command:

```powershell
docker exec -it HASH bash
```

Example:

```powershell
docker exec -it 9fc51bd46c3634abc63dde17d46d89f677d4b6a83602ec10651e919785a31f9a bash
```

Now you have a shell inside the container.

### 5. Edit & Build

You can edit the code inside the `./project` folder and the changes will be immediately visible in your container.

You can use the container as a standard Ubuntu bash and run gradle as the following commands:

- `gradle build`
- `gradle test`
- ...

### 6. Exiting

Type `exit` into your linux terminal. Afterward you can stop the container or delete it. Definitely stop the Docker process though, to stop it from eating your resources.

### 7. Further on

After building the container for the first time, there is no need to do it over again. Just follow steps again from **4.** for a quick start.

## Enjoy!

Richard
